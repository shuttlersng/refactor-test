const _ = require('lodash');
const moment = require('moment');
const shortid = require('shortid');
const User = require('../../app/models/user');
const UserRoute = require('../../app/models/user_route').UserRoute;
const RoutePickup = require('../../app/models/route_pickup');
const UserRouteSchedule = require('../../app/models/user_route_schedule');
const errors = require('../../app/errors');
const bookshelf = require('../../app/bookshelf');
const WalletComponent = require('../components/wallet');

class UserService {
  /**
   * Set route for a user
   * @param {object} req Restify request object - Contains route ID.
   */

  async route(req) {
    const reqId = shortid.generate();
    const userId = req.params.id;
    const pickUpId = req.body.pickup_id;
    const routeId = req.body.route_id;
    const itineraryId = req.body.itinerary_id;
    const destinationId = req.body.destination_id;
    const startDate = req.body.start_date;
    const daysIds = req.body.days_ids;

    const allowedKeys = [
      'route_id',
      'itinerary_id',
      'pickup_id',
      'destination_id',
      'recurring',
      'start_date',
      'end_date',
      'meta',
      'with_return_trip'
    ];

    const routeData = _.pick(req.body, allowedKeys);

    // fetch all route days by their id provided by the client
    const routeDays = await this.getRouteDays(daysIds, routeId)
      .where('seats_available', '>', 0)
      .fetchAll({ withRelated: 'day' });

    let selectedDays = routeDays.map(routeDay =>
      routeDay.related('day').get('name')
    )
    this.logger.info(`User Server - Assign Route - Route Days: ${JSON.stringify(routeDays)} - Selected Days: ${JSON.stringify(selectedDays)} - Selected Days Length - ${selectedDays.length}`);
    let validDaysLength = selectedDays.length;//this.getValidDaysLength(selectedDays);

    if (!routeDays.length) {
      throw new errors.InvalidDays();
    }

    const promises = [];

    let currentWeek = new Date().getWeekNumber();
    let startWeek = new Date(startDate).getWeekNumber();

    routeData.user_id = userId;
    routeData.current_week = startWeek <= currentWeek;

    if (routeData.current_week && !validDaysLength) {
      throw new errors.LateBooking();
    }

    this.logger.info(
      `User App - User Service - route - Request ID: ${reqId} - Started the process of setting route for user with ID: ${userId}`
    );

    let user = User.where({ id: userId.toString() }).fetch({
      withRelated: ['wallet'],
      require: true
    });

    let routePickup = RoutePickup.where({
      id: pickUpId.toString(),
      route_id: routeId.toString()
    }).fetch({
      require: true,
      withRelated: [
        { itineraries: query => query.where('id', itineraryId) },
        { destinations: query => query.where('id', destinationId) },
        'route'
      ]
    });

    return Promise.all([user, routePickup])
      .then(([user, routePickup]) => {
        const wallet = user.related('wallet').get('amount');
        const destination = routePickup.toJSON().destinations[0];
        const itinerary = routePickup.toJSON().itineraries[0]; // Itineraries belonging to this route
        const pickup = routePickup.toJSON().location;

        const busPass = pickup.substring(0, 2).toUpperCase() +
          destination.location.substring(0, 2).toUpperCase() +
          Math.floor(Math.random() * 1000); // TODO use a more random way for generatin the last 3 digits

        // add bus pass to data
        routeData.bus_pass = busPass;

        if (!destination) {
          this.logger.error(
            `User App - User Service - route - Request ID: ${reqId} - Destination: ${destinationId} does not exist: ${JSON.stringify(
              routeData
            )}`
          );

          throw new errors.ItemNotFound('Destination does not exist');
        }

        if (!itinerary) {
          this.logger.error(
            `User App - User Service - route - Request ID: ${reqId} - Itinerary: ${itineraryId} does not exist: ${JSON.stringify(
              routeData
            )}`
          );

          throw new errors.ItemNotFound('Itinery does not exist');
        }

        routeData.cost = routeDays.length * destination.fare;

        // See if this user has enough move in their wallet to set a route
        if (wallet >= routeData.cost) {
          // Save user route details

          return bookshelf.transaction(trx => {
            return new UserRoute(routeData)
              .save(null, { transacting: trx })
              .tap(userRoute => {
                // Save user route schedules
                let formattedUserRoute = userRoute.toJSON();
                formattedUserRoute.selected_days = selectedDays;
                promises.push(formattedUserRoute);

                this.logger.info(
                  `User App - User Service - route - Request ID: ${reqId} - Started the process route for user ${userId}`
                );

                let scheduleData = {};
                scheduleData.user_id = userId;
                scheduleData.route_id = routeId;
                scheduleData.user_route_id = userRoute.id;
                scheduleData.days_ids = JSON.stringify(
                  routeDays.map(routeDay => routeDay.get('day_id'))
                );

                return new UserRouteSchedule(scheduleData)
                  .save(null, {
                    transacting: trx,
                    method: 'insert'
                  })
                  .then(() => {
                    // Reduce number of available seat.
                    this.logger.info(
                      'Reducing the number of seats for the selected days'
                    );

                    routeDays.each(routeDay => {
                      routeDay.save(
                        {
                          seats_available: routeDay.get('seats_available') - 1
                        },
                        { patch: true }
                      );
                    });

                    this.logger.info(
                      'Successfully reduced the available seats in respective days'
                    );

                    // at this poin the transaction is complete, send out notification emails
                    this.logger.info(
                      `User App - Trip Booking Service - create - Request ID: ${reqId} - Preparing booking email notification`
                    );

                    const fname = user.get('fname');
                    const lname = user.get('lname');
                    const userEmail = user.get('email');
                    const userPhone = user.get('phone');

                    const emailTemplateParams = {
                      subject: 'Your booking has been made',
                      name: `${fname} ${lname}`,
                      email: userEmail,
                      amount: routeData.cost,
                      totalAmount: routeData.cost,
                      busPass: routeData.bus_pass,
                      startDate: moment(routeData.start_date).format(
                        'MM-DD-YYYY'
                      ),
                      date: moment().format('YYYY-MM-DD HH:mm:ss'),
                      template: 'booking'
                    };

                    this.queue.publish({
                      message: {
                        type: 'email',
                        params: emailTemplateParams,
                        recipient: userEmail,
                        template: 'booking'
                      }
                    });

                    // notify admin
                    this.queue.publish({
                      message: {
                        type: 'email',
                        params: {
                          subject: 'New Shuttlers booking',
                          email: userEmail,
                          phone: userPhone,
                          name: `${fname} ${lname}`,
                          amount: routeData.cost,
                          withReturn: routeData.with_return_trip || false,
                          totalAmount: routeData.cost,
                          busPass: routeData.bus_pass,
                          startDate: moment(routeData.start_date).format(
                            'MM-DD-YYYY'
                          ),
                          date: moment().format('YYYY-MM-DD HH:mm:ss')
                        },
                        recipient: 'bookings@shuttlers.ng',
                        template: 'notify-admin-booking'
                      }
                    });

                    this.logger.info(
                      `User App - Trip Booking Service - create - Request ID: ${reqId} - Email sent to user for successful booking`
                    );
                  });
              })
              .then(trx.commit)
              .then(() => {
                // only debit users if thier booking was amde for this current week, this was designed to help with autoreniews anc cancellations
                // if (routeData.current_week) {

                //   let cost = validDaysLength * destination.fare;

                //   let description = `Payment for ${routePickup.get('location')} to ${destination.location} for ${validDaysLength} trip(s)`

                //   new WalletComponent(userId).debit(cost, description)
                // }

                let cost = validDaysLength * destination.fare * (routeData.with_return_trip ? 2 : 1);

                let description = `Payment for ${routePickup.get(
                  'location'
                )} to ${destination.location} for ${validDaysLength} trip(s)`;

                if(routeData.with_return_trip) {
                  description += " with return";
                }

                const walletAction = new WalletComponent(userId).debit(
                  cost,
                  description
                );
              })
              .catch(trx.rollback);
          });
        }

        this.logger.error(
          `User App - User Service - route - Request ID: ${reqId} - User ${userId} does not have enough funds in wallet`
        );

        throw new errors.WalletInsufficientFunds(
          'User does not have enough funds in their wallet to book a route.'
        );
      })
      .then(() => {
        return promises.pop();
      })
      .then(final => {
        this.logger.info(
          `User App - User Service - route - Request ID: ${reqId} - Route ${routeId} for user with ID: ${userId} was saved.`
        );
        this.metrics.increment('ride.user.route.success');

        return final;
      })
      .catch(error => {
        this.metrics.increment('ride.user.route.error');

        if (error.message === 'EmptyResponse') {
          this.logger.error(
            `User App - User Service - route - Request ID: ${reqId} - User with ID: ${userId} or Route ${routeId} was not found`
          );

          throw new errors.ItemNotFound('User/Route does not exist');
        }

        if (error.code === 'ER_DUP_ENTRY') {
          this.logger.error(
            `User App - User Service - route - Request ID: ${reqId} - User ${userId} is already on this route: ${JSON.stringify(
              routeData
            )}`
          );
          return {};
        }

        this.logger.error(
          `User App - User Service - route - Request ID: ${reqId} - Error in setting route for user: ${userId} - ${error}`
        );

        throw error;
      });
  }

}
